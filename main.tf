terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}


resource "aws_instance" "petclnicapp" {
  ami           = "ami-08ee2516c7709ea48"
  instance_type = "t2.micro"
  tags = {
    Name = "Petclinic App"
  }
}
