
resource "aws_db_parameter_group" "default" {
  name   = "mysql"
  family = "mysql8"

  parameter {
    name  = "max_allowed_packet"
    value = "16777216"
  }
}

resource "aws_db_subnet_group" "default" {
  name       = "main"
  subnet_ids = [aws_subnet.private_1.id, aws_subnet.private_2.id]

  tags = {
    Name = "My DB subnet group"
  }
}

resource "aws_db_instance" "default" {
  allocated_storage      = 20
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t2.micro"
  name                   = "petclinicdb"
  username               = "elvin"
  password               = "12345678"
  storage_type           = "gp2"
  publicly_accessible    = false
  multi_az               = false
  max_allocated_storage  = 21
  skip_final_snapshot    = true
  identifier             = "petclinicdb"
  db_subnet_group_name   = aws_db_subnet_group.default.name
  vpc_security_group_ids = [aws_security_group.db.id]
  availability_zone      = aws_subnet.private_1.availability_zone
}

output "end_point" {
  value = aws_db_instance.default.endpoint
}
